# Docker Image .NET application builder

### Summary

Make available a docker image that can be used as a GitLab runner to build .NET code

Include this in your `.gitlab-ci.yml` file:

```yml
image: rakaim/dotnetbuilder:latest
```