FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

# Install apt-utils
#RUN apt-get -qqy update \
#    && apt-get install -qqy --no-install-recommends apt-utils \
#    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Install dotnet core data
RUN apt-get -qqy update \
    && apt-get install -qqy \
    nmon \
#    mono-devel \
#    mono-xbuild \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*
    
# Update nuget
#RUN nuget update -self
